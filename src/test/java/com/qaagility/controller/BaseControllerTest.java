package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class BaseControllerTest {

	@Test
	public void testWelcome() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcome(map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - "));
		verify(map).addAttribute(eq("counter"), anyInt());
	}

	@Test
	public void testWelcomeName() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcomeName("test", map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - test -"));
		verify(map).addAttribute(eq("counter"), anyInt());
	}


	@Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        int expected = 10;
        
        assertEquals(expected, result);
    }

	@Test
    public void testDesc() {
        About about = new About();
        String expected = "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
        assertEquals(expected, about.desc());
    }

	@Test
    public void testMul() {
        Calcmul calcmul = new Calcmul();
        int expected = 18;
        assertEquals(expected, calcmul.mul());
    }
   
	@Test
    public void testD() {
        Cnt cnt = new Cnt();
        int expected = Integer.MAX_VALUE;
        assertEquals(expected, cnt.d(1, 0));
    }

    	@Test
    public void testD2() {
        Cnt cnt = new Cnt();
        int expected = 6;
        assertEquals(expected, cnt.d(12, 2));
    }
}
